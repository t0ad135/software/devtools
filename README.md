# DevTools
 This repo is a grab-bag for any software and other project resources. By listing these here we can ensure that everyone on the team is working from the same toolset and prevent inconsistencies.

# Git for Windows
    I _highly_ recommend downloading and installing Git for Windows to make version control easier. Link below:
    [Git for Windows](https://github.com/git-for-windows/git)
